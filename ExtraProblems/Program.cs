﻿using System;
using System.Collections.Generic;

namespace ExtraProblems
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Resolvers.ReverseSentence("This  is  an example!");
            var isOdd = Resolvers.OddEven(new List<int>{2,-1,-4});
            var isEven = Resolvers.OddEven(new List<int>());
        }
    }
}
