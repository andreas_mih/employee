﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtraProblems
{
    public static class Resolvers
    {
        public static string ReverseSentence(string input)
        {
            StringBuilder result = new StringBuilder();
            while(input.Contains(" "))
            {
                var shortV = input.Substring(0, input.IndexOf(" "));
                result.Append(Reverse(shortV)+" ");

                input = input.Substring(input.IndexOf(" ")+1);
            }
            result.Append(Reverse(input));

            return result.ToString();
        }

        public static string OddEven(IList<int> input)
        {
            bool isEven = true;
            foreach (var number in input)
                if (number % 2 != 0)
                    isEven = !isEven;

            return isEven ? "Even" : "Odd";
        }

        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

    }
}
