﻿using employee.Models.ViewModels.Input;
using employee.Services;
using Microsoft.AspNetCore.Mvc;

namespace employee.Controllers
{
    [Route("Api")]
    public class APIController : ControllerBase
    {
        private readonly IEmployeeService _service;
        public APIController(IEmployeeService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("Employee")]
        public IActionResult PostEmployee([FromBody] Employee employee)
        {
            if(employee != null)
            {
                var dbEmployee = Mapper.ToDbEmployee(employee);
                var result = _service.AddEmployee(dbEmployee);
                if (result > 0)
                    return Ok(result);

                return Content("Failed to add");
            }

            return BadRequest("The employee object sent was null");
        }

        [HttpPut]
        [Route("Employee")]
        public IActionResult PutEmployee([FromBody] Models.Database.Employee employee)
        {
            if (employee != null)
            {
                var result = _service.EditEmployee(employee);
                if (result > 0)
                    return Ok();

                return Content("Failed to update");
            }

            return BadRequest("The employee object sent was null");
        }

        [HttpGet]
        [Route("Employee")]
        public IActionResult GetEmployee([FromQuery]int id)
        {
            var employee = _service.Get(id);
            if(employee != null)
                return Ok(employee);

            return BadRequest();
        }
    }
}
