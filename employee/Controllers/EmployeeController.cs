﻿using employee.Models.ViewModels.Input;
using employee.Models.ViewModels.Ouput;
using employee.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace employee.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _service;
        public EmployeeController(IEmployeeService service)
        {
            _service = service;
        }

        // GET: Home
        public ActionResult Index(string searchString)
        {
            var allDbAccounts = _service.GetAll();
            if (!String.IsNullOrEmpty(searchString))
            {
                var nameContain = allDbAccounts.Where(s => s.Name.Contains(searchString));
                var depContain = allDbAccounts.Where(s => s.Department.Contains(searchString));
                allDbAccounts = nameContain.Union(depContain);
            }
            var dispayableAccounts = allDbAccounts.Select(o => new EmployeeView
            {
                Name = o.Name,
                Department = o.Department,
                Address = o.Address,
                Id = o.Id,
                DOB = o.DOB,
                Position = o.Position,
                Salary = o.Salary,
                EnrolmentDate = o.EnrolmentDate,
                Gender = o.Gender
            }).ToList();

            return View(new EmployeePageViewModel(dispayableAccounts));
        }

        public ActionResult Delete(int id)
        {
            if(id > 0)
                _service.DeleteEmployee(id);

            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            var dbEmployee = _service.Get(id);
            return View(new EmployeeView
            {
                Name = dbEmployee.Name,
                Department = dbEmployee.Department,
                Address = dbEmployee.Address,
                Id = dbEmployee.Id,
                DOB = dbEmployee.DOB,
                Position = dbEmployee.Position,
                Salary = dbEmployee.Salary,
                Gender = dbEmployee.Gender,
                EnrolmentDate = dbEmployee.EnrolmentDate
            });
        }

        public ActionResult Add()
        {
            var newEmployee = new Employee();
            newEmployee.DOB = DateTime.UtcNow.AddYears(-20);
            newEmployee.Gender = "";
            return View(newEmployee);
        }


        [HttpPost]
        public ActionResult Add(Employee employee)
        {
            if(ModelState.IsValid && employee != null)
            {
                var dbEmployee = Mapper.ToDbEmployee(employee);

                _service.AddEmployee(dbEmployee);
                return RedirectToAction("Index");
            }

            return View(employee);
        }
    }
}
