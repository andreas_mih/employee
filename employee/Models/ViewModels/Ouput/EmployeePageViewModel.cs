﻿using System.Collections.Generic;

namespace employee.Models.ViewModels.Ouput
{
    public class EmployeePageViewModel
    {
        public EmployeePageViewModel(IList<EmployeeView> employees)
        {
            Employees = employees;
        }
        public IList<EmployeeView> Employees { get; set; }
    }
}
