﻿using System;

namespace employee.Models.ViewModels.Ouput
{
    public class EmployeeView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public decimal Salary { get; set; }
        public string Address { get; set; }
        public string Department { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public DateTime EnrolmentDate { get; set; }
    }
}
