﻿using System;
using System.ComponentModel.DataAnnotations;

namespace employee.Models.ViewModels.Input
{
    public class Employee
    {
        [Required]
        public string Name { get; set; }
        public string Position { get; set; }
        public decimal Salary { get; set; }
        public string Address { get; set; }
        public string Department { get; set; }
        public string Gender { get; set; }
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }
    }
}
