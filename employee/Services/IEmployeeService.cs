﻿using employee.Models.Database;
using System.Collections.Generic;

namespace employee.Services
{
    public interface IEmployeeService
    {
        public int AddEmployee(Employee employee);
        public void DeleteEmployee(int id);
        public int EditEmployee(Employee employee);
        public IEnumerable<Employee> GetAll();
        public Employee Get(int id);
    }
}
