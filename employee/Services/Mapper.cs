﻿using System;

namespace employee.Services
{
    public static class Mapper
    {
        public static Models.Database.Employee ToDbEmployee(Models.ViewModels.Input.Employee employee)
        {
            return new Models.Database.Employee
            {
                Name = employee.Name,
                Department = employee.Department,
                Address = employee.Address,
                Position = employee.Position,
                Salary = (int)employee.Salary,
                DOB = employee.DOB,
                EnrolmentDate = DateTime.UtcNow,
                Gender = employee.Gender
            };
        }
    }
}
