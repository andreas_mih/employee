﻿using employee.Models.Database;
using System.Collections.Generic;
using System.Linq;

namespace employee.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly EmployeeContext _dbContext;
        public EmployeeService(EmployeeContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int AddEmployee(Employee employee)
        {
            if (employee == null)
                return 0;

            var employeeWithSameName = _dbContext.Employees.Where(o => o.Name == employee.Name).FirstOrDefault();
            if(employeeWithSameName == null)
            {
                _dbContext.Employees.Add(employee);
                _dbContext.SaveChanges();
                return employee.Id;
            }

            return 0;
        }

        public void DeleteEmployee(int id)
        {
            if(id > 0)
            {
                var employee = _dbContext.Employees.Find(id);
                if(employee != null)
                {
                    _dbContext.Employees.Remove(employee);
                    _dbContext.SaveChanges();
                }
            }
        }

        public Employee Get(int id)
        {
            if(id > 0)
                return _dbContext.Employees.Find(id);

            return null;
        }

        public int EditEmployee(Employee employee)
        {
            var dbEmployee = _dbContext.Employees.Find(employee.Id);
            _dbContext.Entry(dbEmployee).CurrentValues.SetValues(employee);
            return _dbContext.SaveChanges();
        }


        public IEnumerable<Employee> GetAll()
        {
            return _dbContext.Employees.ToList();
        }
    }
}
